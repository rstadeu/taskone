import React, {Component} from "react";

const Avatar = () => <img src="http://i.pravatar.cc/120"/>;


const UserName = () => <h4>John Doe</h4>;


const Bio = () =>
    <p>
        <strong>Bio: </strong>
        A ajsnndajl asndlakjsq asndanslda aksldalskd iiusada,
        kajsndkaj ajksndiuqjs akajkssd.
    </p>;


const UserProfile = () =>
    React.createElement(
        "div",
        null,
        React.createElement(
            Avatar
        ),
        React.createElement(
            UserName
        ),
        React.createElement(
            Bio
        )
    );

class App extends React.Component{
    constructor(props){
        super(props)

    }

    render(){
        return React.createElement(
            "div",
            null,
            React.createElement(
                "h1",
                {className: "App"},
                "Hello World"
            ),
            React.createElement(
                UserProfile
                )
        );

    }

}

export default App
