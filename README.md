# Task 1 (Core concepts lecture)

Setting up the environment

Install and launch “blank” node.js application. You’ll need it in further development for implementing SSR. Commit the changes to repository.

Don’t use any generators (like create-react-app)

Create components in different ways, using:

- React.createElement

- React.Component

- React.PureComponent

- functional components

Note: To implement this task you could use React online sandbox, [optional] or start to configure basic project build from task 2. Share sandbox link with your mentor or commit to your repository

Evaluation criteria:

(each mark includes previous mark criteria)

|                   2                  |                       3                       |                          4                          |                                  5                                  |
|:------------------------------------:|:---------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------------------------:|
|Install blank express.js application | Render blank message (Hello World) with react | Use at least 2 methods of creating react components | Use all methods which mentioned in task, to create react components |

## Running the project

Install all dependencies
```
npm install
```

To Run 
```
npm run start
```


## Captures from the actual Project
Main Page


![MainScreen](public/MainPage.jpg)